// Jakie wyrażenie da w rezultacie znaczenie „Mango”?
// a. null && false && "Mango" // false
// b. null || "false" || "Mango"
// c. "false" && "Mango" || null
// d. "Mango" && null || false

// operator && zwraca ostatni operand (jeden z warunków) jeżeli całe wyrażenie zwraca true

// operator || zwraca pierwszy operand (jeden z warunków) który "zwraca" true

// console.log(null && false && "Mango"); // null
// console.log(null || "false" || "Mango"); // "false"
// console.log(("false" && "Mango") || null); // Mango
// console.log(("Mango" && null) || false); // false

// tablice - zmienne referencyjne
// const a = [];
// const b = a; // a i b wskazuja na dokladnie to samo miejsce w pamieci

const clients = [
  "Mango", // 0
  "Apple",
  "Apple",
  "Poly", // 1
  "Ajax", // 2
  "Kiwi", // 3
  function () {
    // 4
    console.log("Hello from array");
  },
  {},
  [],
  "Apple",
];
const clients2 = ["Poly", "Ajax", "Kiwi", "Mango"];

// console.log(clients);
// console.log(clients2);

// console.log(clients[0]);
// console.log(clients[1]);
// console.log(clients[2]);
// console.log(clients[3]); //
// console.log(clients[4]); // undefined (brak wartosci na podanym indeksie)
// console.log(clients[5]); // undefined (brak wartosci na podanym indeksie)

// clients[100] = "Jakub";
// console.log(clients);
// let i = 0;
// while (clients[i] !== undefined) {
//   console.log(i);
//   i++;
// }

// if ("Jakub".length === 5) {};
// const person = {
//   name: "Jakub",
//   city: "Poznan",
//   sayHello() {
//     console.log("Hejo!");
//   },
// };

// person.name;
// person.sayHello();
// person["sayHello"]();
// clients[4](); // Hello from array

// clients[0] = "Apple";
// console.log(clients); // tablica z nadpisanym elementem o indeksie 0 (Mango => Apple)
// console.log(
//   clients.findIndex((el) => {
//     return el === "Apple";
//   })
// );

// rekurencja - do poczytania

for (let i = 0; i < clients.length; i++) {
  // console.log(clients[i]);
}

// for ... of
// zmienna - variable
// clients - iterable
// for (const zmienna of clients) {
//   console.log(zmienna);
// }

// const clients = ["Mango", "Poly", "Ajax"];
// const clientNameToFind = "Poly1";
// let message;

// for (const client of clients) {
//   // W każdej iteracji sprawdzamy, czy element tablicy jest podobny
//   // do imienia klienta. Jeśli wszystko się zgadza, wtedy zapiszmy w message
//   // wiadomość o sukcesie i zróbmy break, aby przerwać poszukiwanie
//   if (client === clientNameToFind) {
//     message = "Klient z takim imieniem jest w bazie danych!";
//     break;
//   }

//   // Jeśli nie znajdziemy podobieństw, to zapiszmy do message wiadomość o braku imienia
//   message = "Nie znaleźliśmy takiego klienta w bazie danych!";
// }

// console.log(message); // "Klient z takim imieniem jest w bazie danych!"
const message = "JavaScript jest ciekawy";
// console.log(message.split(" "));
// const clients = ["Mango", "Ajax", "Poly", "Kiwi"];
// console.log(clients.indexOf([])); // 2
// console.log(clients.indexOf("Monkong")); // -1
const numbers = [];

// numbers.push(1);
// console.log(numbers); // [1]

// numbers.push(2);
// console.log(numbers); // [1, 2]

// numbers.push(3);
// console.log(numbers); // [1, 2, 3]

// numbers.push(4);
// console.log(numbers); // [1, 2, 3, 4]

// numbers.push(5);
// console.log(numbers); // [1, 2, 3, 4, 5]

// numbers.unshift(0);
// console.log(numbers); // [0, 1, 2, 3, 4, 5]

// numbers.shift();
// console.log(numbers); // [1, 2, 3, 4, 5]

// numbers.pop();
// console.log(numbers); // [1, 2, 3, 4]

// slice
// console.log(clients.slice(1, 3)); // ["Ajax", "Poly"]
// clients.slice().push("1234");
// console.log(clients.slice().push("1234"));
// console.log(clients.splice(0, 3));
// console.log(clients.splice(2, 1, "Mango", "Cherries"));

// const person = {
//   colors: [''],
//   address: {
//     z: {},
//     k: {}
//   }
// }

// console.log(clients.concat([1, 2, 3]));
// console.log(clients);
let x = 3;
const y = x++;

console.log(`x:${x}, y:${y}`);
// expected output: "x:4, y:3"

let a = 3;
const b = ++a;

console.log(`a:${a}, b:${b}`);
// expected output: "a:4, b:4"
