// 1. Pure functions

// pure / side effects

// pure function - zwraca zawsze ten sam set danych dla takich samych danych wejsciowych

const addTwoNumbers = (a, b) => a + b;
const addTwoNumbersSideEffect = (a, b) => {
  console.log(Date.now());
  return a + b;
};
// console.log(addTwoNumbers(1, 2));
// console.log(addTwoNumbers(1, 2));
// console.log(addTwoNumbers(1, 2));
// console.log(addTwoNumbers(1, 2));
// console.log(addTwoNumbers(1, 2));
// console.log(addTwoNumbers(1, 2));

// console.log(addTwoNumbersSideEffect(1, 2));
// console.log(addTwoNumbersSideEffect(1, 2));
// console.log(addTwoNumbersSideEffect(1, 2));
// console.log(addTwoNumbersSideEffect(1, 2));
// console.log(addTwoNumbersSideEffect(1, 2));
// console.log(addTwoNumbersSideEffect(1, 2));
// console.log(addTwoNumbersSideEffect(1, 2));

// const dirtyMultiply = (array, value) => {
//   for (let i = 0; i < array.length; i += 1) {
//     array[i] = array[i] * value;
//   }
// };

// const numbers = [1, 2, 3, 4, 5];
// dirtyMultiply(numbers, 2);
// Zdarzyła się mutacja oryginalnych danych - tablicy numbers
// console.log(numbers); // [2, 4, 6, 8, 10]

// const pureMultiply = (array, value) => {
//   const newArray = [];

//   array.forEach((element) => {
//     newArray.push(element * value);
//   });

//   return newArray;
// };

// const numbers = [1, 2, 3, 4, 5];
// const doubledNumbers = pureMultiply(numbers, 2);

// // Nie było mutacji oryginalnych danych
// console.log(numbers); // [1, 2, 3, 4, 5]
// // Funkcja zwróciła nową tablicę ze zmienionymi danymi
// console.log(doubledNumbers); // [2, 4, 6, 8, 10]

// DRY - Dont Repeat Yourself

// const callbackFunction = (el, id, arr) => {
//   console.log(el);
// };

// [1, 2, 3, 4, 5].map(callbackFunction);
// [5, 5, 55, 2, 2, 2, 22].map(callbackFunction);

// const func = (callback) => {
//   callback();
// };

// const planets = ["Ziemia", "Mars", "Wenus", "Jowisz"];

// const planetsInUpperCase = planets.map((planet) => planet.toUpperCase());
// console.log(planetsInUpperCase); // ['ZIEMIA', 'MARS', 'WENUS', 'JOWISZ']

// const planetsInLowerCase = planets.map((planet) => planet.toLowerCase());
// console.log(planetsInLowerCase); // ['ziemia', 'mars', 'wenus', 'jowisz']

// // Oryginalna tablica się nie zmieniła
// console.log(planets); // ['Ziemia', 'Mars', 'Wenus', 'Jowisz']

// const students = [
//   { name: "Mango", score: 83 },
//   { name: "Poly", score: 59 },
//   { name: "Ajax", score: 37 },
//   { name: "Kiwi", score: 94 },
//   { name: "Houston", score: 64 },
// ];

// const names = students.map((student) => student.name);

// const numbers = [1, 2, 3, 4, 5, 6];
// const doubledNumbersMapped = numbers.map((number) => number * 2);
// console.log(doubledNumbersMapped);

// const arr = [];
// for (const number of numbers) {
//   arr.push(number * 2);
// }
// console.log(arr);

// const students = [
//   { name: "Mango", courses: ["matematyka", "fizyka"] },
//   { name: "Poly", courses: ["informatyka", "matematyka"] },
//   { name: "Kiwi", courses: ["fizyka", "biologia"] },
// ];

// console.log(students.map((student) => student.courses));
// [['matematyka', 'fizyka'], ['informatyka', 'matematyka'], ['fizyka', 'biologia']]
// const arr = [];
// console.log(
//   students
//     .flatMap((student) => student.courses)
//     .forEach((course) => {
//       if (!arr.includes(course)) arr.push(course);
//     })
// );
// console.log(arr);

// const values = [51, -3, 27, 21, -68, 42, -37, 100000];

// const positiveValues = values.filter((value) => value >= 0);
// console.log(positiveValues); // [51, 27, 21, 42]

// const negativeValues = values.filter((value) => value < 0);
// console.log(negativeValues); // [-3, -68, -37]

// const bigValues = values.filter((value) => value > 1000);
// console.log(bigValues); // []

// // Oryginalna tablica się nie zmieniła
// console.log(values); // [51, -3, 27, 21, -68, 42, -37]

// const students = [
//   { name: "Mango", courses: ["matematyka", "fizyka"] },
//   { name: "Poly", courses: ["informatyka", "matematyka"] },
//   { name: "Kiwi", courses: ["fizyka", "biologia"] },
// ];

// const allCourses = students.flatMap((student) => student.courses);
// // ['matematyka', 'fizyka', 'informatyka', 'matematyka', 'fizyka', 'biologia'];

// const uniqueCourses = allCourses.filter(
//   (course, index, array) => array.indexOf(course) === index
// );
// console.log(uniqueCourses);

// const LOW_SCORE = 50;
// const HIGH_SCORE = 80;
// const students = [
//   { name: "Mango", score: 83 },
//   { name: "Poly", score: 59 },
//   { name: "Ajax", score: 37 },
//   { name: "Kiwi", score: 94 },
//   { name: "Houston", score: 64 },
// ];

// const best = students.filter((student) => student.score >= HIGH_SCORE);
// console.log(best); // Tablica obiektów o nazwach Mango i Kiwi

// const worst = students.filter((student) => student.score < LOW_SCORE);
// console.log(worst); // Tablica z jednym obiektem Ajax

// // W funkcji wywołania zwrotnego wygodnie jest zdestrukturyzować właściwości obiektu
// const average = students.filter(
//   ({ score }) => score >= LOW_SCORE && score < HIGH_SCORE
// );
// console.log(average); // Tablica obiektów o nazwach Poly i Houston

// const colorPickerOptions = [
//   { label: "blue", color: "#abcdef" },
//   { label: "red", color: "#F44336" },
//   { label: "green", color: "#4CAF50" },
//   { label: "blue", color: "#2196F3" },
//   { label: "pink", color: "#E91E63" },
//   { label: "indigo", color: "#3F51B5" },
// ];

// console.log(colorPickerOptions.find((option) => option.label === "blue")); // { label: 'blue', color: '#2196F3' }
// colorPickerOptions.find((option) => option.label === "pink"); // { label: 'pink', color: '#E91E63' }
// console.log(colorPickerOptions.find((option) => option.label === "white")); // undefined

// const colorPickerOptions = [
//   { label: "red", color: "#F44336" },
//   { label: "green", color: "#4CAF50" },
//   { label: "blue", color: "#2196F3" },
//   { label: "pink", color: "#E91E63" },
//   { label: "indigo", color: "#3F51B5" },
// ];

// console.log(colorPickerOptions.findIndex((option) => option.label === "blue")); // 2
// console.log(colorPickerOptions.findIndex((option) => option.label === "pink")); // 3
// console.log(colorPickerOptions.findIndex((option) => option.label === "white")); // -1

// Czy wszystkie elementy są większe lub równe zero? - tak
// console.log([1, 2, 3, 4, 5].every((value) => value >= 0)); // true

// // Czy wszystkie elementy są większe lub równe zero? - nie
// console.log([1, 2, 3, -10, 4, 5].every((value) => value >= 0)); // false

// // Czy jest przynajmniej jeden element większy lub równy zero? - tak
// console.log([1, 2, 3, 4, 5].some((value) => value >= 0)); // true

// // Czy jest przynajmniej jeden element większy lub równy zero? - tak
// console.log([-7, -20, 3, -10, -14].some((value) => value >= 0)); // true

// // Czy jest przynajmniej jeden element mniejszy od zera? - nie
// console.log([1, 2, 3, 4, 5].some((value) => value < 0)); // false

// // Czy jest przynajmniej jeden element mniejszy od zera? - tak
// console.log([1, 2, 3, -10, 4, 5].some((value) => value < 0)); // true

// const fruits = [
//   { name: "apples", amount: 100 },
//   { name: "bananas", amount: 0 },
//   { name: "grapes", amount: 50 },
//   { name: "kiwis", amount: 20 },
//   { name: "mangos", amount: 120 },
// ];

// // every zwróci true tylko wtedy, jeśli będzie więcej niż 0 sztuk wszystkich owoców
// const allAvailable = fruits.every((fruit) => fruit.amount > 0); // false

// // some zwróci true tylko wtedy, jeśli będzie więcej niż 0 sztuk przynajmniej jednego owocu
// const anyAvailable = fruits.some((fruits) => fruits.amount > 0); // true

// const allFruits = fruits.reduce((prev, curr) => {
//   return (prev += curr.amount);
// }, 0);
// console.log(allFruits);

// const students = [
//   { name: "Mango", score: 83 },
//   { name: "Poly", score: 59 },
//   { name: "Ajax", score: 37 },
//   { name: "Kiwi", score: 94 },
//   { name: "Houston", score: 64 },
// ];

// // Nazwa akumulatora może być dowolna, to tylko parametr funkcji
// const totalScore = students.reduce((total, student) => {
//   return total + student.score;
// }, 0);

// // console.log(totalScore / students.length);

// const tweetsArray = [
//   { id: "000", likes: 5, tags: ["js", "nodejs"] },
//   { id: "001", likes: 2, tags: ["html", "css"] },
//   { id: "002", likes: 17, tags: ["html", "js", "nodejs"] },
//   { id: "003", likes: 8, tags: ["css", "react"] },
//   { id: "004", likes: 0, tags: ["js", "nodejs", "react"] },
// ];

// const tweetsArray2 = [
//   { id: "000", likes: 55, tags: ["js", "nodejs"] },
//   { id: "001", likes: 22, tags: ["html", "css"] },
//   { id: "002", likes: 171, tags: ["html", "js", "nodejs"] },
//   { id: "003", likes: 81, tags: ["css", "react"] },
//   { id: "004", likes: 0, tags: ["js", "nodejs", "react"] },
// ];

// // Popatrzmy na wszystkie elementy kolekcji i dodajmy wartości właściwości likes
// // do akumulatora, którego początkowa wartość wynosi 0.
// const likes = tweetsArray.reduce(
//   (totalLikes, tweet) => totalLikes + tweet.likes,
//   0
// );

// // console.log(likes); // 32

// // Liczenie polubień nie jest pojedynczą operacją, więc napiszmy funkcję
// // do zliczania polubień z kolekcji
// const countLikes = (tweetsArrayOfObjects) => {
//   return tweetsArrayOfObjects.reduce((totalLikes, tweet) => {
//     return totalLikes + tweet.likes;
//   }, 0);
// };

// console.log(countLikes(tweetsArray)); // 32
// console.log(countLikes(tweetsArray2)); // 32

// const scores = [61, 19, 74, 35, 92, 56, -1];
// scores.sort(); // mutacja
// console.log(scores); // [19, 35, 56, 61, 74, 92]

// const scores2 = [27, 2, 41, 4, 7, 3, 75];
// scores2.sort();
// console.log(scores2); // [2, 27, 3, 4, 41, 7, 75]

// const students = ["Vika", "Andrey", "Oleg", "Julia", "Boris", "Katya"];
// students.sort();
// console.log(students); // [ 'Andrey', 'Boris', 'Vika', 'Katya', 'Oleg', 'Julia' ]

// const letters = ["b", "B", "a", "A", "c", "C"];
// letters.sort();
// console.log(letters); // ['A', 'B', 'C', 'a', 'b', 'c']

// const scores = [61, 19, 74, 35, 92, 56];
// const ascendingScores = [...scores].sort((a, b) => b - a);

// console.log(scores); // [61, 19, 74, 35, 92, 56]
// console.log(ascendingScores); // [19, 35, 56, 61, 74, 92]

const students = [
  { name: "Poly", score: 59 },
  { name: "Kiwi", score: 94 },
  { name: "Ajax", score: 37 },
  { name: "Mango", score: 83 },
];

const inAscendingScoreOrder = students
  .sort(
    (firstStudent, secondStudent) => firstStudent.score - secondStudent.score
  )
  .map((student) => student.name)
  .filter((name) => name[0].toUpperCase() === "A")[0];

const inDescendingScoreOrder = [...students]
  .sort(
    (firstStudent, secondStudent) => secondStudent.score - firstStudent.score
  )
  .map((student) => student.name);

const inAlphabeticalOrder = [...students]
  .sort((firstStudent, secondStudent) =>
    firstStudent.name.localeCompare(secondStudent.name)
  )
  .map((student) => student.name);

console.log(inAscendingScoreOrder);
// console.log(inDescendingScoreOrder);
// console.log(inAlphabeticalOrder);

// Abstract Syntax Tree
