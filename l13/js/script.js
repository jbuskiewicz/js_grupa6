// Event propagation
const parent = document.querySelector("#parent");
const child = document.querySelector("#child");
const descendant = document.querySelector("#descendant");

parent.addEventListener("click", (e) => {
  // alert("Parent click handler");
  // console.log(e.target);
  // console.log(e.currentTarget);
  console.log(Date.now());
});

child.addEventListener("click", (e) => {
  // alert("Child click handler");
  // console.log(e.target);
  // console.log(e.currentTarget);
  console.log(Date.now());
});

descendant.addEventListener("click", (e) => {
  // e.stopPropagation();
  // alert("Descendant click handler");
  // console.log(e.target);
  // console.log(e.currentTarget);
  console.log(Date.now());
  // console.log(e);
});
// const colorPalette = document.querySelector(".color-palette");
// const output = document.querySelector(".output");

// colorPalette.addEventListener("click", selectColor);

// // This is where delegation «magic» happens
// function selectColor(event) {
//   if (event.target.nodeName !== "BUTTON") {
//     // console.log(event);
//     return;
//   }

//   const selectedColor = event.target.dataset.color;
//   output.textContent = `Selected color: ${selectedColor}`;
//   output.style.color = selectedColor;
// }

// // Some helper functions to render palette items
// createPaletteItems();

// function createPaletteItems() {
//   const items = [];
//   for (let i = 0; i < 60; i++) {
//     const color = getRangomColor();
//     const item = document.createElement("button");
//     item.type = "button";
//     item.dataset.color = color;
//     item.style.backgroundColor = color;
//     item.classList.add("item");
//     items.push(item);
//   }
//   colorPalette.append(...items);
// }

// function getRangomColor() {
//   return `#${getRandomHex()}${getRandomHex()}${getRandomHex()}`;
// }

// function getRandomHex() {
//   return Math.round(Math.random() * 256)
//     .toString(16)
//     .padStart(2, "0");
// }
