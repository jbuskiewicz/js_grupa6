// console.log("Pętle i warunki");
// console.log("mango".indexOf("nam"));

const PASSWORD = 1234;
const LOGIN = "admin";

// console.log(PASSWORD === 1234); // true
// console.log(LOGIN === "admi"); // false
// console.log(PASSWORD === 1234 || LOGIN === "admi"); // true
// console.log(PASSWORD === 1234 && LOGIN === "admi"); // true

// if () {} else {}

// if () {}
// else {}

// console.log("jestesmy za ifem");

// let amount = 0;
// const subscription = "free";

// if (subscription === "pro") {
//   amount = 100;
// } else {
//   amount = 20;
// }

// console.log(amount); // 100

// ////////////////////////

// let amount; // undefined
// const subscription = "premiumm";

// if (subscription === "free") {
//   amount = 0;
// } else if (subscription === "pro") {
//   amount = 100;
// } else if (subscription === "premium") {
//   amount = 500;
// } else {
//   console.log("Invalid subscription type");
// }

// console.log(amount);

const age = 20;
// ternary operator (Elvis ? : )
let type =
  age >= 18
    ? "wartosc przypisana jezeli prawda"
    : "wartosc przypisana jezeli falsz";

// if (age >= 18) {
//   type = "adult";
// } else {
//   type = "child";
// }

// type = age >= 18 ? "true" : "false";

// console.log("====================================");
// console.log(type);
// console.log("====================================");

// const num1 = 5;
// const num2 = 10;
// let biggerNumber;

// if (num1 > num2) {
//   biggerNumber = num1;
// } else {
//   biggerNumber = num2;
// }

// switch
let cost;
const subscription = "free";

switch (subscription) {
  case "free":
    cost = 0;
    break;

  case "pro":
    cost = 100;
    break;

  case "premium":
    cost = 500;
    break;

  default:
    console.log("Invalid subscription type");
}

// console.log(cost);

const mojaZmienna = 1;
// console.log("mojaZmienna = ", mojaZmienna);

const myFunction = () => {
  const mojaZmienna = 2;
  console.log("mojaZmienna = ", mojaZmienna);
};

// myFunction();

// console.log("mojaZmienna = ", mojaZmienna);
const global = "global";

// if (true) {
//   const blockA = "block A";

//   // Możemy użyć globalny + lokalny A
//   console.log(global); // 'global'
//   console.log(blockA); // block A

//   // Zmienne blockB i blockC nie znalezione w dostępnych zakresach.
//   // Pojawi się błąd wykorzystania zmiennej.
//   console.log(blockB); // ReferenceError: blockB is not defined
//   console.log(blockC); // ReferenceError: blockC is not defined

//   if (true) {
//     const blockB = "block B";

//     // Możemy użyć globalny + zewnętrzny A + lokalny B
//     console.log(global); // global
//     console.log(blockA); // block A
//     console.log(blockB); // block B

//     // Zmienna blockC nie znaleziona w dostępnych zakresach.
//     // Pojawi się błąd wykorzystania zmiennej.
//     console.log(blockC); // ReferenceError: blockC is not defined
//   }
// }

let i = 0;
// while (i < 100) { // infinite loop
//   console.log(i);
// }

// while (i < 100) {
//   console.log(i);
//   i++; // inkrementacja o 1, i = i + 1;
//   i += 1; // i++, i = i + 1
// }

// let password = "";

// do {
//   password = prompt("Wprowadź hasło dłuższe niż 4 znaki", "");
// } while (password.length < 5);

// console.log("Hasło zostało wprowadzone: ", password);

const people = ["Jakub", "Adam", "Jasiu", "Urszula"];

// for (let i = 0; i <= 200; i++) {
//   console.log(i); // 0 | 5 | 10 | 15 | 20
// }

for (let i = 0; i < people.length; ++i) {
  console.log(i);
  //   if (people[i].indexOf("u") === -1) {
  //     continue; // przerywa jedynie dany przebieg petli, a nastepnie inkrementuje licznik
  //     break; // przerywa cala petle w momencie wywolania
  //   }
  //   console.log(people[i]);
  //   console.log("lecimy dalej");
}

// DRY = Dont repeat yourself

// 00010101000 "logujKropke"

// "zwykla" vanilla JS
// console.log(addTwoNumber(2, 2));
function addTwoNumber(a, b) {
  return a + b;
}

// arrow function (ES6)
// const logujKropke = () => {
//   console.log(".");
// };
// a();
// console.log(logujKropke());
// console.log(zwroconaWartosc);
