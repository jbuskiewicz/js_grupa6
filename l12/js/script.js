// Events
const button = document.getElementById("add-content-button");

const mainContentContainer = document.getElementById("main-content");

let i = 0;

const onButtonClickHandler = (event) => {
  console.log(event);
  const el = document.createElement("p");
  el.innerText = `Lorem Ipsum ${i}`;
  el.dataset.size = i;
  mainContentContainer.append(el);
  i++;
};

button.addEventListener("click", onButtonClickHandler);

// button.addEventListener("click", () => {
//   console.log("second event listener");
// });

// setTimeout(() => {
//   // obsluga zdarzenia "click" zostanie usunieta po 6sek od momentu załadowania skryptu JS
//   // event loop - tam trafia funkcja ktora podana jest jako pierwszy parametr setTimeout, by po 6sek zostac wywolana
//   button.removeEventListener("click", onButtonClickHandler);
// }, 6000);

const mango = {
  username: "Mango",
  showUsername() {
    console.log(this);
    console.log(`My username is: ${this.username}`);
  },
};

const btn = document.querySelector(".js-btn");

// ✅ Działa
// mango.showUsername();

// ❌ this będzie się odnosić do button jeśli używać showUsername jako callback
// btn.addEventListener("click", mango.showUsername); // nie działa

// // ✅ Nie zapomnij dołączyć kontekstu metod obiektowych
// btn.addEventListener("click", mango.showUsername.bind(mango));

// stopPropagation(); -> zatrzymujemy propagowanie eventu w dół
// preventDefault(); -> zatrzymujemy domyślne zachowanie eventu

const form = document.querySelector("#form");
form.addEventListener("submit", (event) => {
  event.preventDefault(); // wylacza domyslne zachowanie - w tym przypadku wysylke formularza (zachowanie eventu "submit")
  //   console.log(event.target[0].value);
  //   console.log("zatrzymalismy wysylke formularza");
});

// document.addEventListener("keydown", (event) => {
//   event.preventDefault();

//   if ((event.ctrlKey || event.metaKey) && event.code === "KeyS") {
//     console.log("«Ctrl + s» or «Command + s» combo");
//   }
// });

const emailInput = document.getElementById("email");

emailInput.addEventListener("change", () => {
  console.log("change event");
});

emailInput.addEventListener("input", () => {
  console.log("input event");
});

emailInput.addEventListener("blur", () => {
  console.log("blur event");
});

emailInput.addEventListener("focus", () => {
  console.log("focus event");
});

mainContentContainer.addEventListener("click", (event) => {
  console.log(event.target.dataset.size);
  //   event.target.remove();
});
