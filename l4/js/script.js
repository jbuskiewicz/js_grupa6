// const arr = [1, 5, 3, 9, 3];
// console.log(arr.indexOf(3));
// console.log(arr.includes(2));

// arr.concat([2], [3], [5]);
// console.log(arr.concat([2], [3], [5]));

// adres: 0 === adres: 1
// [1, 3, 5] === [5, 1, 3];

// const a = [1, 3, 5];

// -----------------------------
const container = document.getElementById("container");
const button = document.getElementById("createElementButton");
const button2 = document.getElementById("createElementButton2");

function addHtmlElement(buttonNumber) {
  console.log("dodaje element.. (" + buttonNumber + ")", Date.now());
  // console.log(`dodaje element.. ${buttonNumber}`, Date.now());
}

// DRY - Don't repeat yourself

button.addEventListener("click", function () {
  addHtmlElement(1);
});
button2.addEventListener("click", function () {
  addHtmlElement(2);
});

function multiply(x, y, z) {
  // if (x === 0 || y === 0 || z === 0) return "Mnozenie przez 0 daje 0";
  console.log(`Wynikiem mnożenia jest ${x * y * z}`);
  // return "zwrocona wartosc z funkcji";
}

// const wynik = multiply(3, 5, 6);
// console.log(wynik);

// multiply(1, 2, 3);
// multiply(2, 2, 3);
// console.log("console.log pomiedzy funkcjami");
// multiply(3, 2, 3);
// multiply(4, 2, 3);

function createMobilePhone(memory, cpu, screenSize = 5) {
  return {
    phoneMemory: memory,
    phoneCPU: cpu,
    phoneSize: screenSize,
  };
}

// console.log(createMobilePhone(8, 4, 6));

// function currying
function multiply(...args) {
  let total = 1;

  for (const argument of args) {
    // total = total * argument;
    total *= argument;
  }

  return total;
}

// console.log(multiply(1, 2, 3)); //  6
// console.log(multiply(1, 2, 3, 4)); //  24
// console.log(multiply(1, 2, 3, 4, 5)); //  120

const arr1 = [0, 1, 2, 3, 4];
const arr2 = [5, 6, 7, 8, 9];
const arr3 = [...arr1, ...arr2];
// console.log(arr3);

// transpilacja => np. Babel (polifille, kompatybilnosc wsteczna)

// const dzielenieLiczb = function() {

// }

// function dzielenieLiczb(a, b) {
//   if (b < 0) {
//     return "(nie)dzielimy przez 0";
//   }
// }

// const globalValue = 10;

// console.log(globalValue); // 10

// function foo() {
//   console.log(globalValue); // 10
// }

// for (let i = 0; i < 5; i++) {
//   console.log(globalValue); // 10

//   if (i === 2) {
//     console.log(globalValue); // 10
//   }
// }

// function foo() {
//   const a = 20;
//   console.log(a); // 20

//   for (let i = 0; i < 5; i++) {
//     console.log(a); // 20

//     if (i === 2) {
//       console.log(a); // 20
//     }
//   }
// }

// ❌ Błąd! Zmienna a nie jest dostępna w zakresie globalnym
// console.log(a);
// foo();
// for (let i = 0; i < 3; i++) {
//   const a = 1;
//   // ❌ Błąd! Zmienna a nie jest dostępna w tym zakresie
//   console.log(a);
// }

// const a = 10;
// // GLOBAL SCOPE

// function foo() {
//   // SCOPE A
//   const b = 20;
//   console.log(b);
//   for (let i = 0; i < 5; i++) {
//     // SCOPE B
//     const c = 30;
//     if (i === 3) {
//       // SCOPE C
//       const d = 40;
//       console.log(a);
//       console.log(b);
//       console.log(c);
//       console.log(d);
//     }
//   }
// }

// foo();

function bar() {
  console.log("bar");
  baz();
}

function baz() {
  console.log("baz");
}

function foo() {
  console.log("foo");
  bar();
}

foo();

// deklaracja funkcji => alokacja pamięci
// wywołanie funkcji => wywołanie funkcji na stosie wywołań
