// console.log("test"); // 0110110110101010

// JIT - Just In Time
// V8 - silnik JS
// chakra, spidermonkey

// separacja wątków / separation of concerns
// setInterval(() => {
//   console.log("hello world");
// }, 1000);

// Dwa typy zmiennych - prymitywne i referencyjne (zlozone)
// 1. Prymitywnego
// number
// string
// boolean (true/false)
// undefined

// 2. Referencyjne
// Obiekt
// Tablica
// Funkcja
// null

// deklaracja
// (var) / let/const

// hoisting -> JIT
// console.log(a);

// ES 6
let name = "Jakub"; // string // primitive
let city = "Poznan"; // string // primitive
const code = 1234; // number // primitive
let likesCoffee = true; // boolean // primitive
// code = 12345;
// name = "Kuba";
// console.log(name);

var a; // variable // inicjalizacja zmiennej
a = 1; // definicja zmiennej
// console.log(a);

// referencyjne
const people = ["Jakub", "Jan", "Kuba"]; // array/tablica // referencyjna
// console.log(people);
// people.push("Jakub2");
// console.log(people);

// let animals = null;
// console.log(null);

let username;
console.log(typeof username); // "undefined"

let inputValue = null;
console.log(typeof inputValue); // "object"

const quantity = 17;
console.log(typeof quantity); // "number"

const message = "JavaScript is awesome!";
console.log(typeof message); // "string"

const isSidebarOpen = false;
console.log(typeof isSidebarOpen); // "boolean"

const person = {
  name: "Jakub", // pole
  lastName: "Buskiewicz", // pole
  phoneNumber: 1234567, // pole
  introduce() {
    // metoda
    console.log("Hi, my name is Jakub");
  },
};

// prototype chain
// https://chamikakasun.medium.com/javascript-prototype-and-prototype-chain-explained-fdc2ec17dd04

// referencyjne
// tablice - [] - do elementu odnosimy sie po indeksie
// obiekty - {} - do elementu odnosimy sie po kropce np. person.name lub nawiasie kwadratowym np. person['name']
// funkcje - function introduce() {} - sluza do wywolania np. introduce() -> pamietamy o () po nazwie funkcji

const x = 5;
const y = 10;
const z = 5;
console.log("x: ", x, " y:", y, " z:", z);
console.log("x > y:", x > y); // false
console.log("x < y:", x < y); // true
console.log("x < z:", x < z); // false
console.log("x <= z:", x <= z); // true
console.log("x === y:", x === y); // false
console.log("x === z:", x === z); // true
console.log("x !== y:", x !== y); // true
console.log("x !== z:", x !== z); // false

const p = {};
const p2 = p;

// console.log(p === p2);

// const message = "JavaScript is awesome!";
// alert(message);

// const isComing = confirm("Please confirm hotel reservation");
// console.log(isComing); // typ boolean

// const hotelName = prompt("Please enter some number");
// console.log(typeof hotelName);

// console.log(Number.parseInt("5px")); // 5
// console.log(Number.parseInt("12qwe74")); // 12
// console.log(Number.parseInt("12.46qwe79")); // 12
console.log("-----");
// const invalidNumber = Number("qweqwe"); // NaN
// console.log(Number.isNaN(invalidNumber)); // true
console.log((0.17 + 0.24).toFixed(2));
