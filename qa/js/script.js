// console.log("QA session");
/**

1. Callbacki - https://textbook.edu.goit.global/javascript-yk5evp/v2/pl/docs/lesson-07/callbacks/

2. Funkcje strzałkowe - https://textbook.edu.goit.global/javascript-yk5evp/v2/pl/docs/lesson-07/callbacks/

3. OOP - this, obiekty, metody, dziedziczenie, dziedziczenie prototypowe - https://textbook.edu.goit.global/javascript-yk5evp/v2/pl/docs/lesson-05/objects/

5. Tablice - reduce, method chaining, ostatni element (przejść przez zadanie 48/48)

6. Pytanie o nr telefonu z modułu 3 - prośba o wskazanie zadania (nr tel w "")

4. removeItem - zadanie 10/20, moduł 5

*/

// 1. Funkcji
// funkcje to przede wszystkim fragmenty kodu, które mają za zadanie uprościć i skrócić kod naszego programu

// DRY - przy pomocy funkcji korzystamy z reguły Don't repeat yourself

const plusButton1 = document.getElementById("plusButton1");
const plusButton2 = document.getElementById("plusButton2");
const plusButton3 = document.getElementById("plusButton3");
const plusButton4 = document.getElementById("plusButton4");
const plusButton5 = document.getElementById("plusButton5");

const onButtonClick = () => {
  console.log("kliknieto button");
};

plusButton1.addEventListener("click", onButtonClick);
plusButton2.addEventListener("click", onButtonClick);
plusButton3.addEventListener("click", onButtonClick);
plusButton4.addEventListener("click", onButtonClick);
plusButton5.addEventListener("click", onButtonClick);

function createNewUserInSystem(name, lastName, email) {
  console.log(`${name} ${lastName} ${email} - USER CREATED`);
}

// createNewUserInSystem("Jakub", "Jakub", "jakub@mail.com");

// tworzymy/rejestrujemy w pamiec PC funkcje do pozniejszego wywolania
const multiplyTwoNumbers = (a, b) => {
  console.log("mnozenie:", a * b);
  return a * b;
};

const divideTwoNumbers = (a, b) => {
  console.log("dzielenie:", a / b);
  return a / b;
};

function addTwoNumbers(a, b, nextOperation) {
  const sum = a + b;
  nextOperation(a, b);
  return sum;
}

// console.log("dodawanie:", addTwoNumbers(2, 5, multiplyTwoNumbers));
// console.log("dodawanie:", addTwoNumbers(2, 5, divideTwoNumbers));

const appState = {
  user: {},
};

const setUserInAppState = (userDetails) => {
  //
};

const fetchUserDetailsFromDatabase = (login, pwd, successCallback) => {
  // jezeli dane zostaly pobrane => wywolaj successCallback i przekaz tam pobrane dane, aby ustawic stan aplikacji
};

// callback jest wczesniej zarejestrowanym zdarzeniem (funkcja), która wywołujemy w okreslonych okolicznosciach

const addThreeNumbers = (a, b, c) => {
  return a + b + c;
};

// hoisting
// addThreeNumbers(1, 2, 3);

// const logMyNameToConsole = () => {}
// function logMyNameToConsole() {}

/**
 OOP - this, obiekty, metody, dziedziczenie, dziedziczenie prototypowe - https://textbook.edu.goit.global/javascript-yk5evp/v2/pl/docs/lesson-05/objects/
 */

// const person = {
//   name: "Jakub",
//   lastName: "Kowalski",
//   age: 20,
//   mail: "jakub@mail.com",
// };

// person.sayHello = function () {
//   console.log("hello");
// };

// person.sayHello();

const PersonMethods = {
  sayHello: function () {
    console.log("hello");
  },
  sayGoodbye: function () {
    console.log("bye!");
  },
  sleep: function () {
    console.log("sleeping..");
  },
};

function Person(name, age) {
  this.name = name;
  this.age = age;
}

Person.prototype.sayHello = function () {
  console.log("hello");
};

const person1 = new Person("Jakub", 30);
const person2 = new Person("Jakub", 40);

person1.sayHello();

class PersonKlasa {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  sayHello() {
    console.log("hello");
  }
}

const person3 = new PersonKlasa("JakubKlasa", 50);
// person3.sayHello();

// console.log(person1.sayHello === person2.sayHello);

// person1.sayHello();pro

// prototype to nic innego jak własność funkcji JS. prototype sam w sobie to obiekt

// pobieralismy dane
// walidacja
// pobranie danych z bazy
// bylo ustawienie state z userem
// bylo ustawienie state zeby wylaczyc spinner

class AppState {
  constructor(initialState) {
    this.state = { ...initialState };
  }
  setUserName(userName) {
    this.state = { ...this.state, userName };
  }
  setUserMail(userMail) {
    this.state = { ...this.state, userMail };
  }
}

const appState2 = new AppState({ userName: "", userMail: "" });

console.log(appState2);

const reducer = (prev, current) => {
  return (prev += current);
};

console.log([1, 2, 3, 4, 5, 5].reduce(reducer, 0));

const getTotalBalanceByGender = (users, gender) => {
  return users
    .filter((user) => user.gender === gender) // zwraca tablice
    .reduce((prev, current) => {
      return (prev += current.balance);
    }, 0); // zwraca sume
};
const users = [
  {
    name: "Moore Hensley",
    email: "moorehensley@indexia.com",
    eyeColor: "blue",
    friends: ["Sharron Pace"],
    isActive: false,
    balance: 2811,
    gender: "male",
  },
  {
    name: "Sharlene Bush",
    email: "sharlenebush@tubesys.com",
    eyeColor: "blue",
    friends: ["Briana Decker", "Sharron Pace"],
    isActive: true,
    balance: 3821,
    gender: "female",
  },
  {
    name: "Ross Vazquez",
    email: "rossvazquez@xinware.com",
    eyeColor: "green",
    friends: ["Marilyn Mcintosh", "Padilla Garrison", "Naomi Buckner"],
    isActive: false,
    balance: 3793,
    gender: "male",
  },
  {
    name: "Elma Head",
    email: "elmahead@omatom.com",
    eyeColor: "green",
    friends: ["Goldie Gentry", "Aisha Tran"],
    isActive: true,
    balance: 2278,
    gender: "female",
  },
  {
    name: "Carey Barr",
    email: "careybarr@nurali.com",
    eyeColor: "blue",
    friends: ["Jordan Sampson", "Eddie Strong", "Adrian Cross"],
    isActive: true,
    balance: 3951,
    gender: "male",
  },
  {
    name: "Blackburn Dotson",
    email: "blackburndotson@furnigeer.com",
    eyeColor: "brown",
    friends: [
      "Jacklyn Lucas",
      "Linda Chapman",
      "Adrian Cross",
      "Solomon Fokes",
    ],
    isActive: false,
    balance: 1498,
    gender: "male",
  },
  {
    name: "Sheree Anthony",
    email: "shereeanthony@kog.com",
    eyeColor: "brown",
    friends: ["Goldie Gentry", "Briana Decker"],
    isActive: true,
    balance: 2764,
    gender: "female",
  },
];

console.log(
  users
    .filter((user) => user.gender === "male") // zwraca tablice
    .reduce((prev, current) => {
      return (prev += current.balance);
    }, 0) // zwraca sume
);

console.log(users[users.length - 1]); // ostatni element

class Storage {
  constructor(items) {
    this.items = items;
  }

  removeItem(item) {
    const elIndex = this.items.indexOf(item);
    if (elIndex >= 0) {
      this.items.splice(elIndex, 1);
      console.log(this.items);
    }
  }
}

// Change code above this line
const storage = new Storage(["Nanitoids", "Prolonger", "Antigravitator"]);

console.log(storage);
// console.log(storage.getItems()); // ["Nanitoids", "Prolonger", "Antigravitator"]
// storage.addItem("Droid");
// console.log(storage.getItems()); // ["Nanitoids", "Prolonger", "Antigravitator", "Droid"]
// storage.removeItem("Prolonger");
// console.log(storage.getItems()); // ["Nanitoids", "Antigravitator", "Droid"]
storage.removeItem("Nanitoids");
storage.removeItem("Nanitoids");
storage.removeItem("Nanitoids");
storage.removeItem("Nanitoids");
storage.removeItem("Prolonger");
