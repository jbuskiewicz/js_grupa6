// Callbacki - funkcje wywołania zwrotnego

function greet(name) {
  return `Pozdrawiam ${name}.`;
}

// console.log(greet("Mango")); // Pozdrawiam Mango.
// console.log(greet);

function logArrayToConsole(array) {
  for (let element of array) {
    console.log(element);
  }
}

// console.log(greet) // do console.log przekazujemy referencje (po nazwie naszej funkcji silnik JS sprawdza zawartosc pamieci i wyswietla w konsoli jej deklaracje)
// console.log(greet()); // do console.log przekazujemy (zwracamy) wynik dzialania funkcji "greet" - w tym przypadku "Pozdrawiam {parametr}."

function modul1(callback) {
  // pobieramy dane z serwera...
  const data = [
    { id: 1, data: [] },
    { id: 2, data: [] },
  ];
  // jezeli mamy dane z serwera
  // console.log(callback);
  if (data.length) {
    callback(data);
  }
}

// modul1(logArrayToConsole);
// logArrayToConsole zostaje przekazana (nasza funkcja) jako referencja, a wiec w funkcji "modul1" mamy gotową do uruchomienia funkcję pod nazwą "callback" (logArrayToConsole to argument funkcji, który zostaje przypisany do parametru callback)

function modul1WithTwoCallbacks(successCallback, errorCallback) {
  // pobieramy dane z serwera...
  const data = [
    { id: 1, data: [] },
    { id: 2, data: [] },
  ];
  // jezeli mamy dane z serwera
  // console.log(callback);
  if (data.length) {
    // if (data.length) jest rownoznaczne z if (data.length > 0)
    successCallback([...data]);
    // successCallback przyjmuje postac logArrayToConsole jezeli wypadniemy w ten warunek
  } else {
    // jezeli tablica data nie bedzie zawierala danych (length === 0) - wywolamy funkcje przekazana jako errorCallback (w tym przypadku console.error)
    // errorCallback przyjmuje postac console.error
    errorCallback("wystapil blad pobierania danych"); // console.error("wystapil blad pobierania danych")
  }
}

// modul1WithTwoCallbacks(logArrayToConsole, console.error);
// modul1WithTwoCallbacks(console.log, console.error);

// 1. definiujemy funkcje f1, która jako argument przyjmuje inna funkcje (callback) c1
// 2. wywolujemy zdefiniowaną funkcję f1 i jako argument przekazujemy wczesniej zdefiniowana funkcje f2 (inna niz f1)
// 3. wewnatrz f1 wywolujemy c1, ktory wskazuje na przekazany argument (w tym przypadku f2)

// ------------------------------------------

// function registerGuest(name, callback) {
//   console.log(`Rejestracja gościa ${name}.`);
//   callback(name);
// }

// // Przekaż funkcję inline greet jako wywołanie zwrotne
// registerGuest("Mango", function greet(name) {
//   console.log(`Pozdrawiam ${name}.`);
// });

// // Przekaż funkcję inline notify jako wywołanie zwrotne
// registerGuest("Poly", function notify(name) {
//   console.log(
//     `Szanowny(a) ${name}, Pana/Pani numer będzie gotowy za 30 minut.`
//   );
// });

const numbers = [5, 10, 15, 20, 25];

// Klasyczny for
// for (let i = 0; i < numbers.length; i += 1) {
//   console.log(`Indeks ${i}, wartość ${numbers[i]}`);
// }

// Iterujący forEach
// numbers.forEach(function (value, index) {
//   console.log(`Indeks ${index}, wartość ${value}`);
//   // 0: value => 5, index => 0 (1 wywolanie, indeks 0)
//   // 1: value => 10, index => 1 (2 wywolanie, indeks 1)
//   // ...
// });

function standardFunc(param1, param2, ...args) {
  console.log(param1);
  console.log(param2);
  console.log(args);
  console.log(arguments);
}

const arrayFunc = (param1, param2, ...args) => {
  console.log(param1);
  console.log(param2);
  console.log(args);
  // console.log(arguments);
};

// Zamiana function na array function:
// 1. slowko function zastepujemy const
// 2. po nazwie funkcji wpisujemy znak rownosci
// 3. po nawiasie okraglym (parametrach) dodajemy strzalke =>

// standardFunc(1, 2, 3, 4, 5);
// console.log("----------");
// arrayFunc(1, 2, 3, 4, 5);

// ------------------------------------

function addTwoNumbers(a, b) {
  return a + b;
}

const addTwoNumbersArrayFunc = (a, b) => {
  return a + b;
};
// w uproszczeniu:
// const addTwoNumbersArrayFunc = (a, b) => a + b;

// console.log(addTwoNumbers(1, 2));
// console.log("----------");
// console.log(addTwoNumbersArrayFunc(1, 2));

const multiplyByTen = (a) => {
  return a * 10;
};
// w uproszczeniu:
// const multiplyByTen = a => a * 10;

// ----------------
// numbers.forEach((value, index) =>
//   console.log(`Indeks ${index}, wartość ${value}`)
// );

modul1WithTwoCallbacks(
  (jakisArgm) => {},
  (jakisArgm) => {}
);

// Array function:
// wyrazenie funkcyjne
// brak tablicy "arguments"
// charakteryzuje sie => zapisywana po liscie parametrow (param1, param2) => { // cialo funkcji }

// funkcja: pobierzAktualneDane()

// button.addEventListener('click', () => { // przekazujemy anonimowa array function
// krok 1
// krok 2
// })

// button.addEventListener('click', myFunction) // referencyjne przekazanie funkcji
// button.addEventListener('click', () => {
// myFunction(param);
// })
// button.addEventListener('click', function () {}) // anonimowa funkcja (nie array function, "zwykla" funkcja)

/**
 * Napisz funkcję getProductPrice(productName) która przyjmuje jeden parametr productName - nazwę produktu. Funkcja szuka obiektu produktu o tej nazwie (właściwość name) w tablicy products i zwraca jego cenę (właściwość price). Jeśli nie zostanie znaleziony żaden produkt o tej nazwie, funkcja powinna zwrócić null.
 */

function getProductPrice(productName) {
  const products = [
    { name: "Radar", price: 1300, quantity: 4 },
    { name: "Scanner", price: 2700, quantity: 3 },
    { name: "Droid", price: 400, quantity: 7 },
    { name: "Grip", price: 1200, quantity: 9 },
  ];
  for (const product of products) {
    if (product.name === productName) {
      return product.price;
    }
  }
  return null;
}

// console.log(getProductPrice("Radar"));
// console.log(getProductPrice("Scanner"));
// console.log(getProductPrice("Droid"));
// console.log(getProductPrice("TV"));

/**
 * Klient chce, aby każda mikstura była reprezentowana nie tylko nazwą, ale także ceną, a w przyszłości może mieć inne cechy. Dlatego teraz tablica obiektów o następujących właściwościach będzie przechowywana we właściwości potions.
 * {
  name: "Dragon breath",
  price: 700
}
 */

const atTheOldToad = {
  potions: [
    { name: "Speed potion", price: 460 },
    { name: "Dragon breath", price: 780 },
    { name: "Stone skin", price: 520 },
  ],
  // Change code below this line
  getPotions() {
    return this.potions;
  },
  addPotion(newPotion) {
    for (const potion of this.potions) {
      if (potion.name === newPotion.name) {
        console.log(
          `Error! Potion ${potion.name} is already in your inventory!`
        );
        return `Error! Potion ${potion.name} is already in your inventory!`;
      }
    }
    this.potions.push(newPotion);
  },
  removePotion(potionName) {
    return (this.potions = [
      ...this.potions.filter((potion) => potion.name !== potionName),
    ]);
  },
  updatePotionName(oldName, newName) {
    this.potions = [
      ...this.potions.map((potion) => {
        if (potion.name === oldName) {
          return { ...potion, name: newName };
        } else {
          return { ...potion };
        }
      }),
    ];
  },
  // Change code above this line
};

[
  { id: 1, name: "potion1" },
  { id: 2, name: "potion2" },
].map((element) => element.name);

// atTheOldToad.addPotion({ name: "Dragon breath", price: 780 });
// atTheOldToad.removePotion("Dragon breath");
// console.log(atTheOldToad.getPotions());
