// paradygmaty
// programowanie funkcyjne / programowanie obiektowe

// programowanie funkcyjne / proceduralne
const baseSalary = 30000;
const overtime = 10;
const rate = 20;

const getWage = (baseSalary, overtime, rate) => {
  return baseSalary + overtime * rate;
};

// console.log(getWage(baseSalary, overtime, rate));

// programowanie obiektowe - OOP - Object Oriented Programming
const employee = {
  baseSalary: 30000,
  overtime: 10,
  rate: 20,
  getWage() {
    return this.baseSalary + this.overtime * this.rate;
  },
};

const employee3 = {
  baseSalary: 30000,
};

// console.log(employee.getWage());

// Obiekt
// Klasy
// Interfejsy
// Klasa jest opisem przedmiotu (obiektu), który zostanie utworzony. Obiekt utworzony na bazie tego opisu (tej klasy) jest jej instancją. Komunikacja (obsługa) obiektu jest realizowana przez interfejs.

const employee2 = Object.create(employee);
// console.log(employee2.baseSalary);
// console.log(employee2);

// console.log(employee2 === employee);

// const animal = {
//   legs: 4,
// };
// const dog = Object.create(animal);
// dog.name = "Mango";

// console.log(dog.hasOwnProperty("name")); // true
// console.log(dog.name); // 'Mango'

// console.log(dog.hasOwnProperty("legs")); // false
// console.log(dog.legs); // 4

// console.log(dog);

// const animal = { eats: true };
// const dog = Object.create(animal);
// dog.barks = true;

// for (const key in dog) {
//   console.log(key); // barks, eats
// }

// const animal = {
//   eats: true,
// };
// const dog = Object.create(animal);
// dog.barks = true;

// for (const key in dog) {
//   if (!dog.hasOwnProperty(key)) continue;

//   console.log(key); // barks
// }

const animal = {
  eats: true,
};
const dog = Object.create(animal);
dog.barks = true;

const dogKeys = Object.keys(dog);

// console.log(dogKeys); // ['barks']

// Object.create() przyjmuje jako argument obiekt, na bazie którego tworzony jest nowy obiekt. Własności przekazanego obiektu zostaną wykorzystane jako [[Prototype]] nowego obiektu, ale nie będą uwzględnione w przestrzeni jego własności (hasOwnProperty zwróci false).

// pętla for..in iteruje przez wszystkie własności obiektu (również te z prototypu)

// Metoda statyczna Object.keys() zwróci tylko klucze właściwe obiektu (pominie prototyp)

// pollyfills - jedno z zastosowań

// -------------------------------

// function User(name, email, id, login) {
//   const obj = {};
//   obj.name = name;
//   obj.email = email;
//   obj.id = id;
//   obj.login = login;
//   return obj;
// }

// console.log(User("", "", "", ""));
// console.log(User("", "", "", ""));
// console.log(User("", "", "", ""));
// console.log(User("", "", "", ""));
// console.log(User("", "", "", ""));

// słówko class to tzw. syntax sugar z ES6
// class User {
//   #email;

//   constructor({ name, email, id, login }) {
//     this.name = name;
//     this.#email = email;
//     this.id = id;
//     this.login = login;
//   }

//   // Metoda getEmail
//   getEmail() {
//     return this.#email;
//   }

//   // Metoda changeEmail
//   changeEmail(newEmail) {
//     this.#email = newEmail;
//   }
// }

// const mango = new User({
//   name: "Mango",
//   email: "mango@gmail.com",
//   id: 1,
//   login: "mangomango",
// });
// // mango.#email = "12345";
// console.log(mango);

// // console.log(mango); // {}
// console.log(mango.getEmail()); // mango@gmail.com
// mango.changeEmail("newMangoMail@gmail.com");
// console.log(mango.getEmail()); // newMangoMail@gmail.com

// class User {
//   #email;

//   constructor({ name, email }) {
//     this.name = name;
//     this.#email = email;
//   }

//   // Getter email
//   get email() {
//     return this.#email;
//   }

//   // Setter email
//   set email(newEmail) {
//     this.#email = newEmail;
//   }
// }

// const mango = new User({ name: "Mango", email: "mango@mail.com" });
// console.log(mango.email); // mango@mail.com => getter, pobieranie
// mango.email = "mango@supermail.com"; // setter, ustawianie nowej wartosci
// console.log(mango.email); // mango@supermail.com

// class User {
//   // Deklaracja i inicjalizacja właściwości statycznej
//   static Roles = {
//     ADMIN: "admin",
//     EDITOR: "editor",
//     READER: "reader",
//     MODERATOR: "moderator",
//   };

//   #email;
//   #role;

//   constructor({ email, role }) {
//     this.#email = email;
//     this.#role = role;
//   }

//   get role() {
//     return this.#role;
//   }

//   set role(newRole) {
//     if (!User.Roles[newRole]) {
//       console.error("Role not found");
//       return;
//     }
//     this.#role = newRole;
//   }
// }

// const mango = new User({
//   email: "mango@mail.com",
//   role: User.Roles.ADMIN,
// });
// mango.role = "MODERATOR";
// console.log(mango);

// class User {
//   static #takenEmails = [];

//   static isEmailTaken(email) {
//     return User.#takenEmails.includes(email);
//   }

//   #email;

//   constructor({ email }) {
//     this.#email = email;
//     User.#takenEmails.push(email);
//   }
// }

// const mango = new User({ email: "mango@mail.com" });

// console.log(User.isEmailTaken("poly@mail.com"));
// console.log(User.isEmailTaken("mango@mail.com"));

class Vehicle {
  constructor(color, maxSpeed) {
    this.color = color;
    this.maxSpeed = maxSpeed;
  }
}

// Vehicle jest w tym przypadku klasa bazowa
// Boat jest w tym przypadku klasa potomna

class Boat extends Vehicle {
  // displacement -> wyporność?
  constructor(color, maxSpeed, length, displacement) {
    super(color, maxSpeed); // wywoluje constructor klasy Vehicle (jako klasy bazowej)
    this.length = length;
    this.displacement = displacement;
  }
}

// const boat = new Boat("red", 220, 8, 10);
// console.log(boat);

// class SuperBoat extends Boat {}
// class SuperSuperBoat extends SuperBoat {}
// gorilla - banana problem

class User {
  #email;

  constructor(email) {
    this.#email = email;
  }

  get email() {
    return this.#email;
  }

  set email(newEmail) {
    this.#email = newEmail;
  }
}

class ContentEditor extends User {
  constructor({ email, posts }) {
    super(email);
    this.posts = posts;
  }

  addPost(post) {
    this.posts.push(post);
  }
}

const editor = new ContentEditor({ email: "mango@mail.com", posts: [] });
console.log(editor); // { email: 'mango@mail.com', posts: [] }
console.log(editor.email); // 'mango@mail.com'
editor.addPost("post-1");
console.log(editor.posts); // ['post-1']

const user = new User("test@mail.com");
// user.addPost("");
console.log(user);

console.log(editor instanceof ContentEditor);
console.log(editor instanceof User);
console.log(editor instanceof Object);
