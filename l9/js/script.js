// "use strict";
// var a = 1;
// var a = 2;
// console.log(this);

// function test() {
//   console.log(this);
// }

// test();

// const person = {
//   name: "Jakub",
//   hello() {
//     console.log(this);
//   },
// };

// person.hello();

function showName() {
  console.log(this);
}

// showName(); // tutaj this wskazuje na obiekt globalny Window, poniewaz funkcja wywolana jest w globalnym scope (główny wątek)

const maciej = {
  username: "Maciej",
  showName: showName,
};

// maciej.showName(); // tutaj this wskazuje na obiekt "maciej", poniewaz funkcja "showName" została przypisana jako metoda obiektu

const maciej2 = {
  username: "Maciej2",
  showName: showName,
};

// maciej2.showName(); // this wskazuje na... ?

const maciej3 = {
  username: "Maciej3",
  showName() {
    console.log(maciej3.username);
  },
};

/**
 * this w funkcji/ globalnym scope => wskazuje na obiekt globalny Window
 * this w obiekcie => wskazuje na ten konkretny obiekt
 * this w klasie => wskazuje na instancje klasy (obiekt)
 */

const bookShelf = {
  authors: ["Bernard Cornwell", "Robert Sheckley"],
  getAuthors() {
    return this.authors; // this w tym przypadku wskazuje na obiekt "bookShelf"
  },
  addAuthor(authorName) {
    this.authors.push(authorName); // this wskazuje na bookShelf, a ze pole authors jest tablicą, to możemy wywołać metodę  push
  },
};

// console.log(bookShelf.getAuthors()); // ["Bernard Cornwell", "Robert Sheckley"]
// bookShelf.addAuthor("Tanith Lee");
// console.log(bookShelf.getAuthors()); // ["Bernard Cornwell", "Robert Sheckley", "Tanith Lee"]

// zasieg globalny
function foo() {
  console.log(this);
}

// foo();

const maciejAgain = {
  username: "This is Maciej",
  showThis() {
    console.log(this);
  },
  showName() {
    console.log(this.username);
  },
};

// maciejAgain.showThis(); // {username: "Maciej", showThis: ƒ, showName: ƒ}
// maciejAgain.showName(); // 'Maciej'

// --------------------
// function showThis() {
//   console.log("this in showThis: ", this);
// }

// Wywołujemy w kontekście globalnym
// showThis(); // this in showThis: Window

// const user = {
//   username: "Mango",
// };

// Zapisujemy link do funkcji do właściwości obiektu
// Zauważ, że to nie jest wywołanie - nie ma ()
// user.showContext = showThis;

// Wywołaj funkcję w kontekście obiektu
// this wskaże na bieżący obiekt, w kontekście
// którego odbywa się wywołanie, a nie na obiekt globalny.
// user.showContext(); // this in showThis: {username: "Mango", showContext: ƒ}

// const customer = {
//   firstName: "Jacob",
//   lastName: "Mercer",
//   getFullName() {
//     return `${this.firstName} ${this.lastName}`;
//   },
// };

// function makeMessage(callback) {
//   // callback() to wywołanie metody getFullName bez obiektu
//   console.log(`Przetwarzanie żądania od ${callback()}.`);
// }

// makeMessage(customer.getFullName);

// -----------------------------

// const showThis = () => {
//   console.log("this in showThis: ", this);
// };

// showThis(); // this in showThis: window

// const user = {
//   username: "Mango",
// };
// user.showContext = showThis;

// user.showContext(); // this in showThis: window

const hotel = {
  username: "Resort hotel",
  showThis() {
    const foo = () => {
      // Strzałki zapamiętują kontekst podczas deklaracji,
      // z zasięgu nadrzędnego
      console.log("this in foo: ", this);
    };

    foo();
    console.log("this in showThis: ", this);
  },
};

// hotel.showThis();
// this in foo: {username: 'Resort hotel', showThis: ƒ}
// this in showThis: {username: 'Resort hotel',showThis: ƒ}

// -------------------------------------------
// call, apply, bind

// call -> pierwszy argument to obiekt, na którym wykonujemy funkcje, a po przecinku przekazujemy argumenty samej funkcji, która na obiekcie jest wyowałana

// apply -> analogicznie do call, z tą różnicą, że argumenty przekazujemy w formie tablicy

function greetGuest(greeting) {
  console.log(`${greeting}, `, this.username);
}

const mango = {
  username: "Mango",
};

const poly = {
  username: "Poly",
};

// greetGuest.call(mango, "Hejka"); // Witaj, Mango.
// greetGuest.call(poly, "Witaj"); // Witaj, Poly.
// greetGuest.apply(mango, ["Witaj"]); // Witaj, Mango.
// greetGuest.apply(poly, ["Witaj"]); // Witaj, Poly.

// ------------

function greet(clientName) {
  return `${clientName}, witaj w «${this.service}».`;
}

const steam = {
  service: "Steam",
};

const steamGreeter = greet.bind(steam);
steamGreeter("Mango"); // "Mango, witaj w «Steam»."

const gmail = {
  service: "Gmail",
};

const gmailGreeter = greet.bind(gmail);
gmailGreeter("Poly"); // "Poly, witaj w «Gmail»."

// bind
// najpierw nadajemy kontekst naszej funkcji, przypisujemy wynik tej operacji do zmiennej, a nastepnie wywolujemy nasza funkcje z odpowiednimi argumentami

const customer = {
  firstName: "Jacob",
  lastName: "Mercer",
  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  },
};

function makeMessage(callback) {
  // callback() to wywołanie metody getFullName bez obiektu
  console.log(`Przetwarzanie żądania od ${callback()}.`);
}

// makeMessage(customer.getFullName); // Wystąpi błąd podczas wywoływania funkcji
makeMessage(customer.getFullName.bind(customer));

// bind, call, apply - zmiana kontekstu wywolania funkcji.
// call, apply - od razu wywoluja nasza funkcje
// bind - tworzy dowiazanie, ktore przypisujemy do zmiennej, aby pozniej wywolac nasza funkcje

// zwykle funkcje -> this definiowane w momencie wywolania
// funkcje strzalkowe -> this definiowane w momencie deklaracji
// bind, call, apply -> zmiana kontekstu
// this -> w zaleznosci od miejsca wywolania wskazuje na obiekt gl. window lub obiekt
