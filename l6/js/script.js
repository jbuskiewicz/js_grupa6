// GIT - repozytorium
// GITHUB/GITLAB/BITBUCKET - serwisy - usługa

// -------
// REST - reszta
// SPREAD - "rozsmarowanie"

// 1,2,3,4,5
function sumNumbers(a = 1, b, ...args) {
  let sum = a + b;
  for (const number of args) sum += number;
  return sum;
}
const numbersArray = [1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 9, 10];
// console.log("Suma podanych liczb: ", sumNumbers(...numbersArray));

// ------------------------------------------------------

function logArrayToConsole(...args) {
  // for of - iteracja po tablicy
  for (const element of args) console.log(element);
}

const dataArray = [
  { title: "Shawshank Redemption" },
  [1, 2, 3],
  1,
  2,
  "text",
  { name: "Jakub" },
  false,
  undefined,
  null,
];

// logArrayToConsole(...dataArray);
// console.log(dataArray);
// console.log([{}, {}, ...dataArray]);
// console.log(dataArray);

// [...jakasZdefiniowanaTablica]; // => kopia tablicy "jakasZdefiniowanaTablica"

const favBook = {
  title: "Thinking fast and slow",
  description: "Book about cognitive biases",
  year: 1990,
  author: "Daniel Kahneman",
  category: "Psychology",
};

// console.log(favBook);
const book2 = { ...favBook, title: "", description: "", awards: [] };
// console.log(book2);

// Zastosowanie:
// REST: przypisanie wszystkich argumentow do iterowalnej zmiennej
// SPREAD: wyciagamy wszystkie element z pudelka (tworzymy nowe tablice, nowe obiekty, przekazujemy np. elementy tablicy jako argumenty funkcji (po kolei))

// const numbersArray2 = [1, 2, 3, 4];
// const numbersArray3 = numbersArray2;
// console.log(numbersArray2 === numbersArray3); // true
// console.log(numbersArray2 === [...numbersArray2]); // false => [...numbersArray2] to dynamiczna (nowa) tablica, do której kopiujemy zawartość tablicy numbersArray2

// Destrukturyzacja - obiekty, tablice
const book = {
  title: "The Last Kingdom",
  author: "Bernard Cornwell",
  genres: ["historical prose", "adventure"],
  isPublic: true,
  rating: 8.38,
  a: null,
  b: null,
  c: null,
  d: null,
  e: null,
  g: null,
  h: null,
  i: null,
};
// console.log(Object.keys(book));

// const title = book.title;
// const author = book.author;
// const genres = book.genres;
// const isPublic = book.isPublic;
// const rating = book.rating;
const { author, genres, isPublic, rating, title } = book;

const arrayOfBooks = [book, book, book, book, book];
const arrayOfBooks2 = [book, book, book, book, book];

// for (const el of arrayOfBooks) {
//   const { a, b, c, d, e, g, h, i, title, author, genres, isPublic } = el;
//   for (const el2 of arrayOfBooks2) {
//     const {
//       a: a2,
//       b: b2,
//       c,
//       d,
//       e,
//       g,
//       h,
//       i,
//       title: title2,
//       author,
//       genres,
//       isPublic,
//     } = el2;
//   }
//   //   console.log(a);
//   //   console.log(author);
//   //   console.log(b);
//   //   console.log(c);
//   //   console.log(el);
//   //   console.log(el);
//   //   console.log(el);
//   //   console.log(el);
// }

// const {klucz: nowaNazwa} = obiekt;

const user = {
  name: "Jacques Gluke",
  tag: "jgluke",
  stats: {
    followers: 5603,
    views: 4827,
    // likes: 1308,
  },
};

const {
  name,
  tag,
  stats: { followers, views: profileViews, likes = 0 },
} = user;
// console.log(likes);

// Destrukturyzacja tablic
const rgb = [200, 255, 100];
// const [red, green, blue] = rgb;
// const red = rgb[0];
// const green = rgb[1];
// const blue = rgb[2];

// console.log(red, green, blue);

const [red, ...colors] = rgb;
console.log(...colors);

const arr = [];

const button = document.getElementById("button");

button.addEventListener("click", () => {
  arr.push(1);
  console.log(arr);
});

console.log(arr);
