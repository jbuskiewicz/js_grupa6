function calculateTotalPrice(order) {
  let total = 0;
  // Change code below this line

  for (let i = 0; i < order.length; i += 1) {
    total += order[i];
    // 1: total = total (0) + 12 (order[0])
    // 2: total = total (12) + 85 (order[1])
  }

  // Change code above this line
  return total;
}

// console.log(calculateTotalPrice([12, 85, 37, 4]));
// 1: i = 0; total = 12
// 2: i = 1; total = 97
// 3: i = 2; total = 134
// 4: i = 3; total = 138

function calculateTotalPrice2(order) {
  let total = 0;
  // Change code below this line
  for (const arrayItem of order) {
    total += arrayItem;
    // 0: arrayItem = 12
    // 1: arrayItem = 85
    // 2: arrayItem = 37
    // 3: arrayItem = 4
  }

  // Change code above this line
  return total;
}

// console.log(calculateTotalPrice2([12, 85, 37, 4]));

// ---------------------
// Obiekty

// -------
// skill - parametr
// function learn (skill) {}

// 'javascript' - argument
// learn('javascript');
// -------

const person = {
  name: "Jasiu",
  lastName: "Buskiewicz",
  skills: [],
  age: 60,
  address: {},
  learn(skill) {
    person.skills.push(skill);
  },
};

const person2 = {
  name: "Jakub",
  lastName: "Buskiewicz",
  skills: [],
  age: 60,
  contactDetails: {
    email: "",
    phone: "",
  },
  address: {
    work: {
      city: "Poznan",
      street: "Krolowej Jadwigi",
    },
    home: {
      city: "Poznan",
      street: "Grunwaldzka",
    },
    familyHome: {},
  },
  visitedPlaces: [{}, {}, {}],
  learn(skill) {
    person.skills.push(skill);
  },
};

// person.learn("javascript");
// console.log(person);

// wlasnosci (pola i metody) obiektow - odwolujemy sie do nich po kropce (sposób nr 1)

// console.log(person2.name);
// console.log(person2.lastName);
// console.log(person2.age);
// console.log(person2.address.work.city);
// console.log(person2);

const o = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
  e: 5,
};

const name = "c";
// console.log(o[name]);

// object -> methods, properties (props)
person2.age = 45;
// console.log(person2.age);

person2.hobbies = [];
person2.hobbies.push("music", "books");
person2.country = "Poland";
// person2["books"]["favourite"] = "Thinking fast and slow";
// console.log("====================================");
// console.log(person2);
// console.log("====================================");

// funkcja konstruktora
function createPerson(name, lastName) {
  const obj = {};
  // obj["name"] = name;
  obj.name = name;
  obj["lastName"] = lastName;
  return obj;
}

// console.log(createPerson("Jim", "Carrey"));

const firstName = "Jakub2";
const lastName = "Buskiewicz";

const myData = {
  firstName: firstName,
  lastName: lastName,
};

// console.log(myData);

// destrukturyzacja + labelling
// const { firstName: secondFirstName, lastName: secondLastName } = myData;
// console.log(secondFirstName);
// console.log(secondLastName);

// kontekst wywołania
// const myArr = [1, 2, 3, 4, 5];
// console.log(myArr);

function intro() {
  // console.log(this);
  // this traktujemy jako kontekst, w którym funkcja została wywołana
  // w zaleznosci od miejsca wywolania funkcji (scope globalny, obiekt) - this bedzie wskazywalo na rozne obiekty (Window lub konkretny obiekt)
  console.log(`Hello, my name is ${this.name} ${this.lastName}`);
}
const person3 = {
  name: "Daniel",
  lastName: "Kahneman",
};

// person2.introduce = intro;
// person3.introduce = intro;
// person2.introduce();
// person3.introduce();

// intro();

const book = {
  title: "The Last Kingdom",
  author: "Bernard Cornwell",
  genres: ["historical prose", "adventure"],
  rating: 8.38,
};

for (const wlasnoscObiektu in person2) {
  // Klucz
  //   console.log(key);
  //   if (wlasnoscObiektu === "country") console.log(person2[wlasnoscObiektu]);
  // Wartość właściwości z tym kluczem
  //   console.log(book[key]);
}

const keys = Object.keys(book);
const values = Object.values(book);
// console.log(book);
// console.log(keys); // ['title', 'author', 'genres', 'rating']
// console.log(values); //

const entries = Object.entries(book);
// console.log(entries);

for (const kluczWartosc of entries) {
  //   console.log(`${kluczWartosc[0]}: ${kluczWartosc[1]}`);
}

const newBook = Object.create(book);
newBook.title = "Wladca pierscieni";
// console.log(newBook);
// console.log(newBook.title);
// console.log(newBook.hasOwnProperty("title"));
// console.log(newBook.hasOwnProperty("author"));

const books = [
  {
    title: "The Last Kingdom",
    author: "Bernard Cornwell",
    rating: 8.38,
  },
  {
    title: "Beside Still Waters",
    author: "Robert Sheckley",
    rating: 8.51,
  },
  {
    title: "Sen śmiesznego człowieka",
    author: "Fiodor Dostojewski",
    rating: 7.75,
  },
];
let avgRating = 0;
for (const book of books) {
  avgRating += book.rating;
}

console.log((avgRating / books.length).toFixed(2));
